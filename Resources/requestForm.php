<?php
/*
	Template Name: RequestForm
*/
if(isset($_GET['formID'])) {
	$searcher = new DrugInfo();

	$id = $_GET['formID'];
	$results = $searcher->getRequestByID($id);
	$requestorCat = array_fill(0, 7, false);
	foreach($results['requestorCat'] as $r) {
		$i = intval($r['RequestorCategory']);
		$requestorCat[$i] = true;
		if($i == 7) {
			$requestorCat[$i] = $r['OtherDescription'];
		}
		
	};

	$requestCat = array_fill(0, 13, false);
	foreach($results['requestCat'] as $r) {
		$i = intval($r['RequestCategoryID']);
		$requestCat[$i] = true;
		if($i == 13) {
			$requestCat[$i] = $r['OtherDescription'];
		}
		
	};

	if ($results['formData']['0']['PreferredContact'] == 'Phone') {
		$phone = true;
	}
	else {
		$phone = false;
	}
	if ($results['formData']['0']['PreferredContact'] == 'eMail') {
		$email = true;
	}
	else {
		$email = false;
	}
	if ($results['formData']['0']['PreferredContact'] == 'Fax') {
		$fax = true;
	}
	else {
		$fax = false;
	}
}

get_header();
get_sidebar();

?>
				

		
		<?php do_action( 'spacious_before_body_content' ); ?>

	<div id="primary">
		<div id="content" class="clearfix">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
	<?php spacious_sidebar_select(); ?>

	<?php do_action( 'spacious_after_body_content' ); ?>
	

	<div class="drugInfoForm">
		<form class="drugInfoForm" action="/wp-content/themes/spacious-child/processRequest.php">
				<input type="hidden" name="requestID" value="<?php echo $results['formData']['0']['ID'] ?>">
				<div class="oneLine">
					<label for="RTBName">Request Taken By:</label>
					<input type="text" name="RTBName" id="RTBName" class="textBox" value="<?php if(isset($results['formData']['0']['TakerName']) && isset($results['formData']['0']['Creation DateTime'])){ echo $results['formData']['0']['TakerName']."@".$results['formData']['0']['Creation DateTime']; } else {echo $_SESSION['ul_user'];}?>" readonly="true">

					<label for="Rname" class="lab">Requestor Name: </label> 
					<input type="text" name="Rname" id="Rname" class="textBox" value="<?php echo $results['formData']['0']['RequestorName'] ?>">
					
					<label for="Rorg" class="lab">Requestor Organization: </label>
					<input type="text" name="Rorg" id="Rorg" class="textBox" value="<?php echo $results['formData']['0']['RequestorOrg'] ?>">
					<br>
				</div>
				
				<div class="oneLine">
					<label for="Remail" class="lab">Requestor Email: </label>
					<input type="text" name="Remail" id="Remail" class="textBox" value="<?php echo $results['formData']['0']['RequestoreMail'] ?>">
					
					<label for="Rfax" class="lab">Requestor Fax: </label>
					<input type="text" name="Rfax" id="Rfax" class="textBox" value="<?php echo $results['formData']['0']['RequestorFax'] ?>">
					<br>
				</div>
				
				<div class="oneLine">	
					<label for="Rphone" class="lab">Requestor Phone: </label>
					<input type="text" name="Rphone" id="Rphone" class="textBox" value="<?php echo $results['formData']['0']['RequestorPhone'] ?>">
					<br>
				</div>
					
				<label class="lab">Preferred Method of Contact: </label>
				<input type="radio" name="PMC" value="eMail" id="eMail" checked="<?php echo $email; ?>"><label for="eMail">e-Mail </label>
				<input type="radio" name="PMC" value="Fax" id="Fax" checked="<?php echo $fax; ?>"><label for="Fax"checked="<?php echo $email; ?>"> Fax </label>
				<input type="radio" name="PMC" value="Phone" id="Phone"checked="<?php echo $phone; ?>"><label for="Phone">Phone </label> 
				<br>

			</div>	
			<hr>

			<!-- Request info -->
			<div id="requestInformation">
				<div id="requestCat">
					<label class="labelClass">Request Category:</label>
					<br>
					<input type="checkbox" name="pid" value="pid" id="pid"<?php if ($requestCat[0] == true){?> checked="checked" <?php } ?>><label for="pid">Product ID</label> <br>
					<input type="checkbox" name="comp" value="comp" id="comp"<?php if ($requestCat[1] == true){?> checked="checked" <?php } ?>><label for="comp">Compounding</label> <br>
					<input type="checkbox" name="inter" value="inter" id="inter"<?php if ($requestCat[2] == true){?> checked="checked" <?php } ?>><label for="inter">Interactions</label><br>
					<input type="checkbox" name="adr" value="adr" id="adr"<?php if ($requestCat[3] == true){?> checked="checked" <?php } ?>><label for="adr">ADR </label><br>
					<input type="checkbox" name="preg" value="preg" id="preg"<?php if ($requestCat[4] == true){?> checked="checked" <?php } ?>><label for="preg">Pregnancy/Lactation</label><br>
					<input type="checkbox" name="drug" value="drug" id="drug"<?php if ($requestCat[5] == true){?> checked="checked" <?php } ?>><label for="drug">Drug/Lab</label> Test <br>
					<input type="checkbox" name="nat" value="nat" id="nat"<?php if ($requestCat[6] == true){?> checked="checked" <?php } ?>><label for="nat">Natural Products</label><br>
					<input type="checkbox" name="dos" value="dos" id="dos"<?php if ($requestCat[7] == true){?> checked="checked" <?php } ?>><label for="dos">Dosing</label><br>
					<input type="checkbox" name="off" value="off" id="off"<?php if ($requestCat[8] == true){?> checked="checked" <?php } ?>><label for="off">Off-Label Drug Use</label><br>
					<input type="checkbox" name="dis" value="dis" id="dis"<?php if ($requestCat[9] == true){?> checked="checked" <?php } ?>><label for="dis">Disease State</label><br>
					<input type="checkbox" name="lit" value="lit" id="lit"<?php if ($requestCat[10] == true){?> checked="checked" <?php } ?>><label for="lit">Literature Search</label><br>
					<input type="checkbox" name="law2" value="law2" id="law2"<?php if ($requestCat[11] == true){?> checked="checked" <?php } ?>><label for="law2">Law </label><br>
					<input type="checkbox" name="art" value="art" id="art"<?php if ($requestCat[12] == true){?> checked="checked" <?php } ?>><label for="art">Article Pull</label><br>

					<label for="rcother" class="otherLabel">Other:</label>
					<input type="text" name="rcother" id="rcother"value="<?php echo $requestCat[13] ?>">
				</div>
				<div id="requesterCat">
					<label class="labelClass">Requestor Category:</label>
					<br>
					<input type="checkbox" name="pharm" value="pharm" id="pharm"<?php if ($requestorCat[0] == true){?> checked="checked" <?php } ?>><label for="pharm">Pharmacist</label><br>
					<input type="checkbox" name="phys" value="phys" id="phys"<?php if ($requestorCat[1] == true){?> checked="checked" <?php } ?>><label for="phys">Physician</label><br>
					<input type="checkbox" name="nurse" value="nurse" id="nurse"<?php if ($requestorCat[2] == true){?> checked="checked" <?php } ?>><label for="nurse">Nurse</label> <br>
					<input type="checkbox" name="cons" value="cons" id="cons"<?php if ($requestorCat[3] == true){?> checked="checked" <?php } ?>><label for="cons">Consumer</label> <br>
					<input type="checkbox" name="stud" value="stud" id="stud"<?php if ($requestorCat[4] == true){?> checked="checked" <?php } ?>><label for="stud">Student</label><br>
					<input type="checkbox" name="alum" value="alum" id="alum"<?php if ($requestorCat[5] == true){?> checked="checked" <?php } ?>><label for="alum">ONU Alumni</label> <br>
					<input type="checkbox" name="law" value="law" id="law"<?php if ($requestorCat[6] == true){?> checked="checked" <?php } ?>><label for="law">Law Enforcement</label><br>

					<label for="rrcother" class="otherLabel">Other:</label>
					<input type="text" name="rrcother" id="rrcother" value="<?php echo $requestorCat[7] ?>">
				</div>
			</div>

			<div class="breakPoint">
				<br>
				<hr>
			</div>

			<!-- Infomation enetered by the user, Where the magic happens -->
			<div id="userEntered">	
	
				<div class="textAreaIn">	
					<label for="backinfo">Background Information:</label> <br>
					<textarea name="backinfo" class="textArea" id="backinfo"><?php echo $results['formData']['0']['BackgroundInfo'] ?></textarea> 
				</div>
				<br>

				<div class="textAreaIn">
					<label for="actquest">Actual Question of Requestor: </label><br>
					<textarea name="actquest"  class="textArea" id="actquest"><?php echo $results['formData']['0']['RequestorQuestion'] ?></textarea>
				</div>
				<br>

				<div class="textAreaIn" id="responNeedBy">
					<label>Response Needed By: </label><br>
					<textarea name="rnbdate" id="rnbdate" class="smallTextArea"><?php echo $results['formData']['0']['ResponseNeededDate'] ?></textarea>
				</div>
				<br>

				<div class="textAreaIn">
					<label for="ssinfo"> Search Strategy and Information Found: </label><br>
					<textarea name="ssinfo"class="textArea" id="ssinfo" ><?php echo $results['formData']['0']['SearchStrategy'] ?></textarea>
				</div>
				<br>
				

				
				<div class="textAreaIn">
					<label for="summary">Summary of Response: </label><br>
					<textarea name="summary" class="textArea" id="summary" ><?php echo $results['formData']['0']['ResponseSummary'] ?></textarea>
				</div>
				<br>

				
			</div>

			<div id="submitArea">
				<input type="submit" name="save" value="Save/Update" class="button">
				<input type="submit" name="approval" value="Submit for Approval" class="button">
				<br>
				
			</div>

		</form>

		</div>
<?php get_footer(); ?>