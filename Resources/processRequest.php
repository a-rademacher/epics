<?php
include_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );

if(isset($_GET['Rname'])) {
    if(isset($_GET['requestID']) && !empty($_GET['requestID'])) {
        updateRequest();
    }
    else {
        newRequest();
    }
}

function newRequest() {
    global $wpdb;
    $wpdb->show_errors();
    $null = null;
    if ($_GET['sfa']) {
        $sfa = date("Y-m-d H:i:s");

    }
    else $sfa = "''";
    $sql = "INSERT INTO `RequestFormData` (`Creation DateTime`, `TakerName`, `RequestorName`,
     `RequestorOrg`, `RequestorPhone`, `RequestorFax`, `RequestoreMail`, `PreferredContact`, `BackgroundInfo`,
      `RequestorQuestion`, `ResponseNeededDate`, `ResponseSummary`, `SubmittedForApproval`,
       `ApprovalPharmacist`, `ApprovalDatetime`) VALUES 
    (now(), '" . /*_SESSION['ul_users'] .*/ "','" . $_GET['Rname'] . "','" .  $_GET['Rorg'] . "',
        '" .  $_GET['Rphone'] . "','" . $_GET['Rfax'] . "','" .  $_GET['Remail'] . "', '" .  $_GET['PMC'] . "',
        '" .  $_GET['backinfo'] . "','" .  $_GET['actquest'] . "','" . $_GET['rnbdate'] . "',
        '" .  $_GET['summary'] . "'," .  $sfa . ", '" . $_GET['pharmapp'] . "',
         '" .  $_GET['appdate'] . "');";

    $wpdb->query($sql);
     $wpdb->print_error(); 


    //insertRequestorCategory();
    //insertRequestCategory();
    echo "Thank you. A new request has been made";
}

function updateRequest() {
    global $wpdb;
    $null = $null;

    if ($_GET['sfa']) {
        $sfa = "now()";
    }
    else $sfa = "''";

    $sql = "UPDATE 'requestformdata' SET 'RequestorName'='" . $_GET['Rname'] . "',
    'RequestorOrg'='" .  $_GET['Rorg'] . "', 'RequestorPhone'='" .  $_GET['Rphone'] . "',
    'RequestorFax'='" . $_GET['Rfax'] . "', 'RequestoreMail'='" .  $_GET['Remail'] . "',
     'PreferredContact' = '" . $_GET['PMC'] . "', 'BackgroundInfo'='" .  $_GET['backinfo'] . "',
     'RequestorQuestion'='" .  $_GET['actquest'] . "', 'ResponseNeededDate'='" . $_GET['rnbdate'] . "',
      'ResponseSummary'='" .  $_GET['summary'] . "', 'SubmittedForApproval'='" .  $sfa . "', 
    'ApprovalPharmacist'='" . $_GET['pharmapp'] . "', 'ApprovalDatetime'='" .  $_GET['appdate'] . "'
     WHERE ID = '" . $_GET['requestID'] . "';";

     $wpdb->query($sql);
     echo "Thank you. The request has been updated";
}

function removeRequest() {

}

function searchRequest() {
	
}

function insertRequestorCategory() {
    if(isset($_GET['pharm']) && !empty($_GET['pharm'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '0', '" . $_GET['pharm'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['phys']) && !empty($_GET['phys'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '1', '" . $_GET['phys'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['nurse']) && !empty($_GET['nurse'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '2', '" . $_GET['nurse'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['cons']) && !empty($_GET['cons'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '3', '" . $_GET['cons'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['stud']) && !empty($_GET['stud'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '4', '" . $_GET['stud'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['alum']) && !empty($_GET['alum'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '5', '" . $_GET['alum'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['law']) && !empty($_GET['law'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '6, '" . $_GET['law'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['rrcother']) && !empty($_GET['rrcother'])) {
        $sql = "INSERT INTO 'requestorcategories' (RequestFormID, RequestorCategory, OtherDescription)
        VALUES ( , '7', 'Other' ,'" . $_GET['rrcother'] . "');";
         $wpdb->query($sql);
    }

}

function insertRequestCategory() {
    if(isset($_GET['pid']) && !empty($_GET['pid'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '0', '" . $_GET['pid'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['comp']) && !empty($_GET['comp'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '1', '" . $_GET['comp'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['inter']) && !empty($_GET['inter'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '2', '" . $_GET['inter'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['adr']) && !empty($_GET['adr'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '3', '" . $_GET['adr'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['preg']) && !empty($_GET['preg'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '4', '" . $_GET['preg'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['drug']) && !empty($_GET['drug'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '5', '" . $_GET['drug'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['nat']) && !empty($_GET['nat'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '6', '" . $_GET['nat'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['dos']) && !empty($_GET['dos'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '7', '" . $_GET['dos'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['off']) && !empty($_GET['off'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '8', '" . $_GET['off'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['dis']) && !empty($_GET['dis'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '9', '" . $_GET['dis'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['lit']) && !empty($_GET['lit'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '10', '" . $_GET['lit'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['law2']) && !empty($_GET['law2'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '11', '" . $_GET['law2'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['art']) && !empty($_GET['art'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategoryID, OtherDescription)
        VALUES ( , '12', '" . $_GET['art'] . "');";
         $wpdb->query($sql);
    }
    if(isset($_GET['rcother']) && !empty($_GET['rcother'])) {
        $sql = "INSERT INTO 'requestcategories' (RequestFormID, RequestCategory, OtherDescription)
        VALUES ( , '13','" . $_GET['rcother'] . "');";
         $wpdb->query($sql);
    }

}